package br.com.amarques.ecommerce.catalog.mapper;

import br.com.amarques.ecommerce.catalog.domain.Supplier;
import br.com.amarques.ecommerce.catalog.dto.SupplierDTO;
import br.com.amarques.ecommerce.catalog.dto.createupdate.CreateUpdateSupplierDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SupplierMapper {

    public static Supplier toEntity(CreateUpdateSupplierDTO dto) {
        Supplier supplier = new Supplier();
        supplier.setName(dto.name);
        return supplier;
    }

    public static SupplierDTO toDTO(Supplier supplier) {
        return new SupplierDTO(supplier.getId(), supplier.getName());
    }
}
