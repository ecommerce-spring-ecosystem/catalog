package br.com.amarques.ecommerce.catalog.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.amarques.ecommerce.catalog.dto.ProductDTO;
import br.com.amarques.ecommerce.catalog.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.catalog.dto.createupdate.CreateUpdateProductDTO;
import br.com.amarques.ecommerce.catalog.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductResource {

    private final ProductService service;

    @PostMapping
    public ResponseEntity<SimpleEntityDTO> create(@Valid @RequestBody CreateUpdateProductDTO dto) {
        log.debug("REST request to create a new Product: {}", dto);

        SimpleEntityDTO simpleEntityDTO = service.create(dto);

        return new ResponseEntity<>(simpleEntityDTO, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> getAll() {
        log.debug("REST request to get all Products");

        List<ProductDTO> products = service.getAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getById(@PathVariable Long id) {
        log.debug("REST request to get a Product [{0}]", id);

        ProductDTO product = service.getOneById(id);

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> edit(@PathVariable Long id, @Valid @RequestBody CreateUpdateProductDTO dto) {
        log.debug("REST request to update a Product [id: {0}] [dto: {1}]", id, dto);

        service.update(id, dto);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete a Product [id: {0}]", id);

        service.delete(id);

        return ResponseEntity.ok().build();
    }
}
