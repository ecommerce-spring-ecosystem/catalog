package br.com.amarques.ecommerce.catalog.dto;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProductDTO {

    public final Long id;
    public final String name;
    public final String description;
    public final BigDecimal value;
    public final Long supplierId;
}
