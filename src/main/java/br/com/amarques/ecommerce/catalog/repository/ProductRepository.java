package br.com.amarques.ecommerce.catalog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.amarques.ecommerce.catalog.domain.Product;
import br.com.amarques.ecommerce.catalog.domain.Supplier;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findAllBySupplier(Supplier supplier);

}
