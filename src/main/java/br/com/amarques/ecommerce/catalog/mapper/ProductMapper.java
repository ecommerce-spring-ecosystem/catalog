package br.com.amarques.ecommerce.catalog.mapper;

import br.com.amarques.ecommerce.catalog.domain.Product;
import br.com.amarques.ecommerce.catalog.domain.Supplier;
import br.com.amarques.ecommerce.catalog.dto.ProductDTO;
import br.com.amarques.ecommerce.catalog.dto.createupdate.CreateUpdateProductDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductMapper {

    public static Product toEntity(CreateUpdateProductDTO dto, Supplier supplier) {
        Product product = new Product();
        product.setName(dto.name);
        product.setDescription(dto.description);
        product.setValue(dto.value);
        product.setSupplier(supplier);
        return product;
    }

    public static ProductDTO toDTO(Product product) {
        return new ProductDTO(product.getId(), product.getName(), product.getDescription(), product.getValue(),
                product.getSupplier().getId());
    }
}
