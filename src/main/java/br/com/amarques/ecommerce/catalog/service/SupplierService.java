package br.com.amarques.ecommerce.catalog.service;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.amarques.ecommerce.catalog.domain.Supplier;
import br.com.amarques.ecommerce.catalog.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.catalog.dto.SupplierDTO;
import br.com.amarques.ecommerce.catalog.dto.createupdate.CreateUpdateSupplierDTO;
import br.com.amarques.ecommerce.catalog.exception.NotFoundException;
import br.com.amarques.ecommerce.catalog.mapper.SupplierMapper;
import br.com.amarques.ecommerce.catalog.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SupplierService {

    private final SupplierRepository repository;

    @Transactional
    public SimpleEntityDTO create(CreateUpdateSupplierDTO dto) {
        Supplier supplier = SupplierMapper.toEntity(dto);
        repository.save(supplier);
        return new SimpleEntityDTO(supplier.getId());
    }

    @Transactional
    public void delete(Long id) {
        Supplier supplier = findById(id);
        repository.delete(supplier);
    }

    protected Supplier findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageFormat.format("Supplier [id: {0}] not found.", id)));
    }

    @Transactional
    public void update(Long id, CreateUpdateSupplierDTO dto) {
        Supplier supplier = findById(id);
        supplier.setName(dto.name);
        repository.save(supplier);
    }

    public List<SupplierDTO> getAll() {
        List<Supplier> suppliers = repository.findAll();
        if (CollectionUtils.isEmpty(suppliers)) {
            return Collections.emptyList();
        }

        return suppliers.stream().map(SupplierMapper::toDTO).collect(Collectors.toList());
    }

    public SupplierDTO getOneById(Long id) {
        Supplier supplier = findById(id);
        return SupplierMapper.toDTO(supplier);
    }
}
