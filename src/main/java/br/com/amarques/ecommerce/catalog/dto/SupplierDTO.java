package br.com.amarques.ecommerce.catalog.dto;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SupplierDTO {

    public final Long id;
    public final String name;
}
