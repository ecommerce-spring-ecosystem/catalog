package br.com.amarques.ecommerce.catalog.dto.createupdate;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CreateUpdateProductDTO {

    @NotNull
    public final String name;
    @NotNull
    public final String description;
    @NotNull
    public final BigDecimal value;
    @NotNull
    public final Long supplierId;
}
