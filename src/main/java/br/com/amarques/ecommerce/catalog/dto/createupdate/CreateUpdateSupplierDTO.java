package br.com.amarques.ecommerce.catalog.dto.createupdate;

import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CreateUpdateSupplierDTO {

    @NotNull
    public final String name;
}
