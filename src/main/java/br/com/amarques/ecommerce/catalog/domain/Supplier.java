package br.com.amarques.ecommerce.catalog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "suppliers")
@SequenceGenerator(name = "supplier_pk", sequenceName = "supplier_id_seq", allocationSize = 1, initialValue = 1)
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "supplier_pk")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;
}
