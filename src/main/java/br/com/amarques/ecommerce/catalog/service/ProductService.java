package br.com.amarques.ecommerce.catalog.service;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import br.com.amarques.ecommerce.catalog.domain.Product;
import br.com.amarques.ecommerce.catalog.domain.Supplier;
import br.com.amarques.ecommerce.catalog.dto.ProductDTO;
import br.com.amarques.ecommerce.catalog.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.catalog.dto.createupdate.CreateUpdateProductDTO;
import br.com.amarques.ecommerce.catalog.exception.NotFoundException;
import br.com.amarques.ecommerce.catalog.mapper.ProductMapper;
import br.com.amarques.ecommerce.catalog.repository.ProductRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;
    private final SupplierService supplierService;

    @Transactional
    public SimpleEntityDTO create(CreateUpdateProductDTO dto) {
        Supplier supplier = supplierService.findById(dto.supplierId);
        Product product = ProductMapper.toEntity(dto, supplier);
        repository.save(product);
        return new SimpleEntityDTO(product.getId());
    }

    @Transactional
    public void delete(Long id) {
        Product product = findById(id);
        repository.delete(product);
    }

    private Product findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageFormat.format("Product [id: {0}] not found.", id)));
    }

    @Transactional
    public void update(Long id, CreateUpdateProductDTO dto) {
        Product product = findById(id);
        product.setName(dto.name);
        product.setDescription(dto.description);
        product.setValue(dto.value);
        repository.save(product);
    }

    public List<ProductDTO> getAll() {
        List<Product> products = repository.findAll();
        return toDTO(products);
    }

    public ProductDTO getOneById(Long id) {
        Product product = findById(id);
        return ProductMapper.toDTO(product);
    }

    public List<ProductDTO> getAllProductsBySupplier(Long supplierId) {
        Supplier supplier = supplierService.findById(supplierId);
        List<Product> products = repository.findAllBySupplier(supplier);
        return toDTO(products);
    }

    private List<ProductDTO> toDTO(List<Product> products) {
        if (CollectionUtils.isEmpty(products)) {
            return Collections.emptyList();
        }
        return products.stream().map(ProductMapper::toDTO).collect(Collectors.toList());
    }
}
