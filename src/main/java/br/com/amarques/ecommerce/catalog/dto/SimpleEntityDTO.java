package br.com.amarques.ecommerce.catalog.dto;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimpleEntityDTO {

    public final Long id;
}
