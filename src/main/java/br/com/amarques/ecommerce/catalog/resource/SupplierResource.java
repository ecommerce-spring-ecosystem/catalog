package br.com.amarques.ecommerce.catalog.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.amarques.ecommerce.catalog.dto.SimpleEntityDTO;
import br.com.amarques.ecommerce.catalog.dto.SupplierDTO;
import br.com.amarques.ecommerce.catalog.dto.createupdate.CreateUpdateSupplierDTO;
import br.com.amarques.ecommerce.catalog.service.SupplierService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/suppliers")
public class SupplierResource {

    private final SupplierService service;

    @PostMapping
    public ResponseEntity<SimpleEntityDTO> create(@Valid @RequestBody CreateUpdateSupplierDTO dto) {
        log.debug("REST request to create a new Supplier: {}", dto);

        SimpleEntityDTO simpleEntityDTO = service.create(dto);

        return new ResponseEntity<>(simpleEntityDTO, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<SupplierDTO>> getAll() {
        log.debug("REST request to get all Suppliers");

        List<SupplierDTO> suppliers = service.getAll();

        return new ResponseEntity<>(suppliers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupplierDTO> getById(@PathVariable Long id) {
        log.debug("REST request to get a Supplier [{0}]", id);

        SupplierDTO supplier = service.getOneById(id);

        return new ResponseEntity<>(supplier, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> edit(@PathVariable Long id, @Valid @RequestBody CreateUpdateSupplierDTO dto) {
        log.debug("REST request to update a Supplier [id: {0}] [dto: {1}]", id, dto);

        service.update(id, dto);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete a Supplier [id: {0}]", id);

        service.delete(id);

        return ResponseEntity.ok().build();
    }
}
